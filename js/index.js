var id_button = "";
$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover(); 
    $(".carousel").carousel({
        interval:1500
    });

    $("#contacto").on('show.bs.modal',function(e){
        console.log("Abriendo");
        id_button = e.relatedTarget.id;
        $("#"+id_button).prop('disabled',true);
        $("#"+id_button).removeClass('btn-outline-success');
        $("#"+id_button).addClass('btn-outline-dark');
        
    });

    $("#contacto").on('shown.bs.modal',function(e){
        console.log("Terminó de abrir");
    });

    $("#contacto").on('hide.bs.modal',function(e){
        console.log("Cerrando");
        console.log(e);
    });

    $("#contacto").on('hidden.bs.modal',function(e){
        console.log("Terminó de cerrar");
        $("#"+id_button).prop('disabled',false);
        $("#"+id_button).removeClass('btn-outline-dark');
        $("#"+id_button).addClass('btn-outline-success');
    });
})